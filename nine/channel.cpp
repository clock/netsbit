#include <iostream>
#include <cstring>
//#include <sys/types.h>
#include <sys/socket.h> // сокеты, бинды, ...
#include <arpa/inet.h> //inet_addr
#include <unistd.h>
#include <ctime>
#include <cerrno>
#include <netdb.h>
#define TIMEOUT_MS 100
#define SERVER 0
#define CLIENT 1


using std::cout;
using std::cin;
using std::getline;
using std::string;

// https://stackoverflow.com/questions/15712821/c-error-undefined-reference-to-classfunction раздельная компиляция
// http://www.cyberforum.ru/post5972945.html
// http://www.cyberforum.ru/cpp-networks/thread1125443.html



class mppp{
public:
	typedef struct icmp_header{
		unsigned char ucType;
		unsigned char ucCode;
		unsigned short int usChecksum;
		unsigned short int usID;
		unsigned short int usSeq;
		char cData[];
	}ICMPHeader;
	
	mppp(int type = 2, char *ip_new = nullptr);
	void client();
	void server();
	void setip(char *ip);
	unsigned short int checkSUM(unsigned short int *, int);
private:
	bool secondary;
	int sockit, packetSize, iICMPDataSize;
	struct sockaddr_in addr, addr2;
	unsigned short int check_port(string buffer); // переводит из строки в номер с проверкой на пригодность
	string buffer;
};


unsigned short int mppp::checkSUM(unsigned short int *buf, int size){
	int nleft = size;
	int sum = 0;
	unsigned short int *w = buf;
	unsigned short int answer = 0;
	while(nleft > 1){
			sum += *w++;
			nleft -= 2;
	}
	if(nleft > 0){
			sum += (char)*w;
	}
	while(sum >> 16) sum = (sum & 0xFFFF) + (sum >> 16);
	answer = (unsigned short int) ~sum;
	return answer;
}



mppp::mppp(int type, char *ip_new){
	addr.sin_port = htons(0);
	
	while(type == 2){
		type = 0;
		static char type_new = '0';
		cout << "What type of object do you prefer server[0]/client[1](default server): ";
		type_new = cin.get();
		if(type_new == '1') type = 1;
		else{
			if(type_new != '0' && type_new != '\n'){
				type = 2;
				cout << "Incorrect value. Try again\n";
				while( type_new = cin.get() != '\n' );
			}
		}
	}
	secondary = (bool)type;
	
	
	if(ip_new) setip(ip_new);
	else if(secondary){
		cout << "ERROR: IP address required\n";
		exit(0);
		// можно добавить предложение о вводе
	}
	
	sockit = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if(sockit < 0){
		perror("socket creating");
		exit(0);
	}
	addr.sin_family = AF_INET;
	iICMPDataSize = 32;
	if(secondary) client();
	else server();
	close(sockit);
}



void mppp::client(){
	
	char buffer_new[2048];
	int n, garanty = 0, current_pid = getpid(), len_buffer = 0;
	unsigned short int checkS=0;
	
	cout << current_pid << '\n';
	
	socklen_t len = sizeof(struct sockaddr_in);
	
	ICMPHeader origin;
	/*
	struct timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	*/
	
	
	while(1){
		garanty = 0;
		cout << "Enter something: ";
		getline(cin, buffer);
		len_buffer = buffer.length();
		if( (iICMPDataSize-1) < len_buffer ) len_buffer = iICMPDataSize;
		for(int i = 0; i < len_buffer; ++i){
			*(origin.cData+i) = buffer[i];
		}
		
		origin.ucType = 8;
		origin.ucCode = 0;
		origin.usID = (unsigned short int) current_pid;
		origin.usSeq = 0;
		origin.usChecksum = 0;
		origin.usChecksum = checkSUM((unsigned short *)&origin, sizeof(origin)+len_buffer);
// 		setsockopt(sockit, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
		do{
			++garanty;
			n = sendto( sockit, (char *)&origin, sizeof(origin)+len_buffer, 0, (struct sockaddr *)&addr, len );
			cout << "Send: " << n << '\n';
			if(garanty > 4){
				cout << "Server is unreacheable\n";
				break;
			}
			/*
		if( origin.ucCode == 8 )
			n = recvfrom(sockit, buffer_new, 2047, 0, (struct sockaddr *)&addr, &len);*/
		}while( n < 0);
		if(garanty < 4){
			cout << "Received: " << n << '\n';
		}
	}
}



void mppp::server(){
	char buffer_new[2048], *iterator;
	int n;
	addr.sin_addr.s_addr=INADDR_ANY;
	if(bind(sockit, (struct sockaddr *)& addr, sizeof(addr)) < 0){
		perror("binding");
		exit(0);
	}
	else cout << "Binded\n";
	
	socklen_t len = sizeof(struct sockaddr_in);
	while(1){
// 		len_buffer = 0;
		n = recvfrom(sockit, buffer_new, 2047, 0, (struct sockaddr *)&addr2, &len);
		if(*(buffer_new+20) != 0)
			cout << "Received: " << n << '\n';
		iterator = buffer_new + n;
		n -= 20+sizeof(ICMPHeader);
		iterator -= n;
		if(*(buffer_new+20) == 8){
			cout << "Received string: "; //<< buffer_new+20+sizeof(ICMPHeader) << '\n';
			for(int i = 0; i < n; ++i) cout << *(iterator+i);
			cout << '\n';
		}
	}
}



void mppp::setip(char *ip){
	struct hostent *host;
	
	if (inet_addr(ip) != INADDR_NONE)
		addr.sin_addr.s_addr = inet_addr(ip);
	else{
		host = gethostbyname(ip);
		if ( host /*= gethostbyname(ip)*/ )
			addr.sin_addr = *(struct in_addr *) host->h_addr_list[0];
		else{
			herror("Incorrect address");
		}
	}
}

