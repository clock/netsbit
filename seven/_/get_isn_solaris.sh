#!/usr/bin/env bash
n=1000
tcpdump -i lo -c $n '(dst port 44444) and (tcp[tcpflags] & (tcp-syn) != 0)' > tmp_file.txt
cat tmp_file.txt | grep seq | sed -r -e 's/.*seq\s([0-9]*).*/\1/g' > isn_list_solaris.txt
rm -f tmp_file.txt
