#!/usr/bin/env bash
n=1000
nohup tcpdump -i lo -c $n '(dst port 44444) and (tcp[tcpflags] & (tcp-syn) != 0)' > tmp_file.txt &
sleep 3
for i in $(seq $n)
    do
    nc localhost 44444
    done
sleep 5
cat tmp_file.txt | grep seq | sed -r -e 's/.*seq\s([0-9]*).*/\1/g' > isn_list_linux.txt
rm -f tmp_file.txt
